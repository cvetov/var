package com.example.mebel;


import java.sql.SQLException;

public class Connect {
    protected ConnectDB db = new ConnectDB();
    protected int counterId;

    Connect() throws SQLException {
        this.initTable();
    }

    void initTable() {
        try {
            this.db.execute("CREATE TABLE IF NOT EXISTS category (\n  id INT(11) NOT NULL AUTO_INCREMENT,\n  name VARCHAR(45) NOT NULL,\n  PRIMARY KEY (id))\nENGINE = InnoDB\nAUTO_INCREMENT = 5\nDEFAULT CHARACTER SET = utf8;");
            this.db.execute("CREATE TABLE IF NOT EXISTS role (\n  id INT(11) NOT NULL AUTO_INCREMENT,\n  name VARCHAR(45) NOT NULL,\n  PRIMARY KEY (id))\nENGINE = InnoDB\nAUTO_INCREMENT = 4\nDEFAULT CHARACTER SET = utf8;");
            this.db.execute("CREATE TABLE IF NOT EXISTS employee (\n  id INT(11) NOT NULL AUTO_INCREMENT,\n  fio VARCHAR(100) NOT NULL,\n  age INT(11) NOT NULL,\n  adress VARCHAR(100) NOT NULL,\n  phone VARCHAR(45) NOT NULL,\n  login VARCHAR(111) NOT NULL,\n  password VARCHAR(45) NOT NULL,\n  idRole INT(11) NOT NULL,\n  last_login DATETIME NULL DEFAULT NULL,\n  PRIMARY KEY (id),\n  CONSTRAINT idRole\n    FOREIGN KEY (idRole)\n    REFERENCES role (id)\n    ON DELETE NO ACTION\n    ON UPDATE NO ACTION)\nENGINE = InnoDB\nAUTO_INCREMENT = 7\nDEFAULT CHARACTER SET = utf8;");
            this.db.execute("CREATE TABLE IF NOT EXISTS status (\n  id INT(11) NOT NULL AUTO_INCREMENT,\n  name VARCHAR(45) NOT NULL,\n  PRIMARY KEY (id))\nENGINE = InnoDB\nAUTO_INCREMENT = 4\nDEFAULT CHARACTER SET = utf8;");
            this.db.execute("CREATE TABLE IF NOT EXISTS orderr (\n  id INT(11) NOT NULL AUTO_INCREMENT,\n  price INT(11) NOT NULL,\n  data DATE NOT NULL,\n  idStatus INT(11) NOT NULL,\n  nameProduct VARCHAR(45) NULL DEFAULT NULL,\n  fioClient VARCHAR(45) NULL DEFAULT NULL,\n  adress VARCHAR(45) NULL DEFAULT NULL,\n  PRIMARY KEY (id),\n  CONSTRAINT idStatus\n    FOREIGN KEY (idStatus)\n    REFERENCES status (id)\n    ON DELETE NO ACTION\n    ON UPDATE NO ACTION)\nENGINE = InnoDB\nAUTO_INCREMENT = 6\nDEFAULT CHARACTER SET = utf8;\n");
            this.db.execute("CREATE TABLE IF NOT EXISTS product (\n  id INT(11) NOT NULL AUTO_INCREMENT,\n  name VARCHAR(70) NOT NULL,\n  price INT(11) NOT NULL,\n  discription VARCHAR(200) NOT NULL,\n  photo VARCHAR(45) NOT NULL,\n  idCategory INT(11) NOT NULL,\n  PRIMARY KEY (id),\n  CONSTRAINT idCategory\n    FOREIGN KEY (idCategory)\n    REFERENCES category (id)\n    ON DELETE NO ACTION\n    ON UPDATE NO ACTION)\nENGINE = InnoDB\nAUTO_INCREMENT = 19\nDEFAULT CHARACTER SET = utf8;\n");
            this.db.execute("CREATE TABLE IF NOT EXISTS order_product ( id INT(11) NOT NULL AUTO_INCREMENT, order_id INT(11) NOT NULL, product_id INT(11) NOT NULL, PRIMARY KEY (id), CONSTRAINT order_id FOREIGN KEY (order_id) REFERENCES orderr (id) ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT product_id FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE NO ACTION ON UPDATE NO ACTION) ENGINE = InnoDB DEFAULT CHARACTER SET = utf8;");
            this.db.execute("INSERT IGNORE INTO category (id, name) VALUES (1, 'Детская '), (2, 'Гостинная'), (3, 'Прихожая'); ");
            this.db.execute("INSERT IGNORE INTO role (id, name) VALUES (1, 'Клиент'), (2, 'Продавец'), (3, 'Администратор'); ");
            this.db.execute("INSERT IGNORE INTO status (id, name) VALUES (1, 'В обработке '), (2, 'В офисе'), (3, 'Готов' );");
            this.db.execute("INSERT IGNORE INTO product (id, name, price, discription, photo, idCategory) VALUES (1, 'Диван красивый', 3899, 'Диван походящий в интерьер. ', 'image11', 1);");
            this.db.execute("INSERT IGNORE INTO orderr (id, price, data, idStatus, nameProduct, fioClient, adress) VALUES (1, 7999, '2022-11-20', 1, 'Insulated men// boots Northland ', 'Andrey Sergeevich Varaksa', 'Nizhny Novgorod'), (2, 479, '2022-11-20', 1, 'Sofa// Cup ', ' Socolov', 'Nizhniy Novgorod'), (3, 4874, '2022-11-21', 1, 'Sofa', 'Vika  Nikitina', 'Lyns'), (5, 599, '2022-11-22', 1, ' World ', 'уыоащда', 'уыоткяжлм' );"  );
            this.db.execute("INSERT IGNORE INTO employee (id, fio, age, adress, phone, login, password, idRole, last_login) VALUES (2, 'Варакса Андрей Сергеевич', 19, 'г.Нижний Новгород', '8955443456', 'var', '12345', 1, '2022-11-22 01:40:48'), (4, 'Маша Васинина Никитина ', 25, 'г.Аразамас', '89045678345', 'sas', '12345', 3, '2022-11-22 02:32:48'), (5, 'Андрей Викторович Высоцкий ', 22, 'г.Нижний Новгород', '89046783254', 'andr', '12345', 2, '2022-11-20 10:19:27'), (6, 'j4wfhaj', 23, 'dkz;jfx', '6894687', '12', '12', 1, '2022-11-22 01:07:26'); ");

        } catch (ClassNotFoundException var2) {
            throw new RuntimeException(var2);
        } catch (SQLException var3) {
            throw new RuntimeException(var3);
        }
    }
}
