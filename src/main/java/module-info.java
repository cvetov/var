module com.example.chocolaterestoran {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires itextpdf;
    //equires mysql.connector.java;


    opens com.example.mebel to javafx.fxml;
    exports com.example.mebel;
}